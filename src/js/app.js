import 'jquery';
import 'slick-slider';
import '@fancyapps/fancybox'
import 'select2'


$(document).ready(function () {

    $('.burger').click(function () {
        let menu = $('.b-menu');

        $(this).toggleClass('rotate');
        menu.toggleClass('show');
    })

    // carousel преподаватели
    $('.b-teachers').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        swipe: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    swipe: true,
                    dots: true,
                }
            },
        ]
    });

    $('.b-teachers-list .teachers-list__item').on('click', function () {
        const pos = $(this).data('position') - 1;
        const carousel = $('.b-teachers');
        const blocks = $('.b-teachers-list .teachers-list__item');

        blocks.removeClass('active');
        $(this).addClass('active');

        carousel.slick('slickGoTo', +pos);
    });

    $('.js-courses').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1139,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    $('.js-tariffs').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1439,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 1139,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    // popup
    $('[data-fancybox]').fancybox();
    
    // popup hidden first, show second
    $('.hide-modal').click(function () {
        $.fancybox.close($('#modal-course'));
        $.fancybox.open($('#course-form'), {
            afterClose: function () {
                $.fancybox.open($('#modal-course'));
            }
        });
    })

    $('.change-modal').click(function () {
        $.fancybox.close($('#select-course'));
        $.fancybox.open($('#course-form-contacts'), {
            afterClose: function () {
                $.fancybox.open($('#select-course'));
            }
        });
    })

    // select2
    $('.js-select2').select2({
        placeholder: 'Выберите курс',
        closeOnSelect: true,
        minimumResultsForSearch: Infinity,
    });

    // collapse
    let coll = document.getElementsByClassName("collapsible__header");

        for (let i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                let content = this.nextElementSibling;
                content.classList.toggle('open');
                if (content.style.maxHeight){
                    content.style.maxHeight = null;
                } else {
                    content.style.maxHeight = (content.scrollHeight + 24) + "px";
                }
            });
        }

    // tabs
    $('.b-tabs .tab-item').on('click', function () {
        let container = $(this).parents('.b-tabs'),
            tabs = container.find('.tab-item'),
            content = container.find('.tab-content'),
            currentTab = $(this).data('tab'),
            currentText = $(this).text(),
            list = container.find('.tabs');

            tabs.removeClass('active');
            $(this).addClass('active');
            list.removeClass('show');

            content.hide();
            $('#' + currentTab).fadeIn(300);
    });

    // adaptive iframes
        let iframes = document.querySelectorAll('.video-frame');
        iframes.forEach(function(i) {
            let iframeWidth = i.width; 
            let iframeHeight = i.height; 
            let iframeParent = i.parentNode; 
            let parentWidth = parseInt(getComputedStyle(iframeParent)['width'])-parseInt(getComputedStyle(iframeParent)['padding-left'])-parseInt(getComputedStyle(iframeParent)['padding-right']); 
            let iframeProportion = parentWidth / iframeWidth;
            i.setAttribute('width', parentWidth); 
            i.setAttribute('height', iframeHeight * iframeProportion); 
        });
});



















